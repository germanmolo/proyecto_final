package com.wilddeer.escaneodedocumentos.utils;

import java.io.File;

public abstract class FileUtils {

    public static boolean deleteFile(String path)
    {
        File file = new File(path);
        return file.delete();
    }
}
