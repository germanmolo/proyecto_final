package com.wilddeer.escaneodedocumentos.utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Objects;

public abstract class NamesFormat {

    public static final String PDF_EXTENSION = ".pdf";
    public static final String IMAGE_EXTENSION = ".jpg";
    public static String formatNombre(String... argumen)
    {
        String result = "";
        String format = "%s ";
        for (int x = 0; x < argumen.length; x++)
        {
            if(x == (argumen.length - 1 ))
                format = "%s";
            result += String.format(format, argumen[x]);
        }
        return result;
    }

    public static String createDocumentNaming(String matricula, String tipoDocumento)
    {
        return String.format("%s_%s", matricula, tipoDocumento.replaceAll("\\s", "_"));
    }

    public static boolean IsJsonString(String test) {
        try {
            new JSONObject(test);
        } catch (JSONException ex) {
            // edited, to include @Arthur's comment
            // e.g. in case JSONArray is valid as well...
            try {
                new JSONArray(test);
            } catch (JSONException ex1) {
                return false;
            }
        }
        return true;
    }
}
