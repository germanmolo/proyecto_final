package com.wilddeer.escaneodedocumentos.classes;

public class Config {
    private int themeOption; //1 claro, 0 oscuro
    private String connectionServer;

    public Config(int themeOption, String connectionServer) {
        this.themeOption = themeOption;
        this.connectionServer = connectionServer;
    }

    public Config(){}

    public int getThemeOption() {
        return themeOption;
    }

    public void setThemeOption(int themeOption) {
        this.themeOption = themeOption;
    }

    public String getConnectionServer() {
        return connectionServer;
    }

    public void setConnectionServer(String connectionServer) {
        this.connectionServer = connectionServer;
    }
}
