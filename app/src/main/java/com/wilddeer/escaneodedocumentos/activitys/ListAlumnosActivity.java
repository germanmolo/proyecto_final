package com.wilddeer.escaneodedocumentos.activitys;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.wilddeer.escaneodedocumentos.R;
import com.wilddeer.escaneodedocumentos.adapters.AlumnosAdapter;
import com.wilddeer.escaneodedocumentos.classes.Alumnos;
import com.wilddeer.escaneodedocumentos.classes.ServerURL;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ListAlumnosActivity extends AppCompatActivity {

    private RecyclerView listAlumnos;
    private AlumnosAdapter adapter;
    RequestQueue queue;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_alumnos);
        initToolbar();
        queue = Volley.newRequestQueue(this);
        listAlumnos = findViewById(R.id.listAlumnos);
        listAlumnos.setLayoutManager(new LinearLayoutManager(this));
        listAlumnos.setHasFixedSize(true);

        consultAlumnos();
    }


    private void initToolbar() {
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Lista de Alumnos");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void consultAlumnos() {

        StringRequest request = new StringRequest(Request.Method.GET,
                ServerURL.consultAlumnos,
                this::loadAlumnos,
                respose -> Log.e("Error", respose.toString()));
        queue.add(request);
    }

    private void loadAlumnos(String response) {
        try {
            ArrayList<Alumnos> list = new ArrayList<>();
            JSONObject object = new JSONObject(response);
            JSONArray array = object.getJSONArray("alumnos");
            for (int x = 0; x < array.length(); x++)
                list.add(new Alumnos(array.getJSONObject(x)));
            adapter = new AlumnosAdapter(list, this);
            adapter.setOnItemClickListener(this::actionItemAlumno);
            listAlumnos.setAdapter(adapter);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("MSG", response);
    }

    private void actionItemAlumno(View view, Alumnos alumnos, int i) {
        Log.e("MDG", String.valueOf(i));
        Intent intent = new Intent(this, PerfilAlumnoActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable(getString(R.string.alumno_index), alumnos);
        intent.putExtras(bundle);
        startActivity(intent);
    }



}
