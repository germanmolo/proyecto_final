package com.wilddeer.escaneodedocumentos.activitys;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.wilddeer.escaneodedocumentos.R;
import com.wilddeer.escaneodedocumentos.adapters.MenuAdapter;
import com.wilddeer.escaneodedocumentos.classes.Menu;
import com.wilddeer.escaneodedocumentos.classes.Usuario;
import com.wilddeer.escaneodedocumentos.database.DBUsuarios;

import java.util.ArrayList;

public class MenuActivity extends AppCompatActivity {

    private Usuario usuario;
    private RecyclerView listMenu;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        usuario = loadUsuario();
        listMenu = findViewById(R.id.listMenu);
        initToolbar();
        loadMenu();

    }

    private void loadMenu() {
        ArrayList<Menu> menuArrayList = new ArrayList<>();
        menuArrayList.add(new Menu("Alumnos", "Listado de alumnos", R.drawable.ic_list,
                R.drawable.wall_menu, v -> openActivity(SearchAlumnosActivity.class)));
        menuArrayList.add(new Menu("Configuracion", "Ajuste de la aplicacion", R.drawable.ic_config,
                R.drawable.wall_menu,v -> openActivity(ConfigActivity.class)));
        menuArrayList.add(new Menu("Acerca de", "Informacion Adicional", R.drawable.warning,
                R.drawable.wall_menu, v -> openActivity(AboutActivity.class)));
        GridLayoutManager manager = new GridLayoutManager(this, 2);

        manager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if (position % 3 == 0)
                    return 2;
                else
                    return 1;
            }
        });


        listMenu.setLayoutManager(manager);
        listMenu.setHasFixedSize(true);
        listMenu.setAdapter(new MenuAdapter(this, menuArrayList));
    }

    private void openActivity(Class aClass)
    {
        Intent intent = new Intent(this, aClass);
        startActivity(intent);
    }

    private Usuario loadUsuario()
    {
        DBUsuarios dbUsuarios = new DBUsuarios(this);
        usuario = dbUsuarios.consultUsuario();
        return usuario;
    }

    private void initToolbar()
    {
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(String.format("Bienvenido %s", usuario.getNombre()));

    }


}
